package decorator.starbuzz;

/**
 * Created by dse on 01/12/2015.
 */
public class Beverage {
    protected double cost;
    protected String description;

    public Beverage(double cost, String desc) {
        this.cost = cost;
        this.description = desc;
    }

    public double cost() {
        return cost;
    }

    public String getDescription() {
        return description;
    }
}
