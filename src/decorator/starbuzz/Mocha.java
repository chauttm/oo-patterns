package decorator.starbuzz;

/**
 * Created by dse on 01/12/2015.
 */
public class Mocha extends Condiment {
    public Mocha(Beverage beverage) {
        super(beverage, .50, "mocha");
    }
}
