package decorator.starbuzz;

/**
 * Created by dse on 01/12/2015.
 */
public class HouseBlend extends Beverage {

    public HouseBlend() {
        super(.99, "");
    }
}
