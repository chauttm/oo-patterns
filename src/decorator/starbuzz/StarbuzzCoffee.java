package decorator.starbuzz;

/**
 * Created by dse on 01/12/2015.
 */
public class StarbuzzCoffee {
    public static void main(String[] args) {
        Beverage myCoffee =
                new Mocha(new Mocha(new Soy(new HouseBlend())));
        Beverage myDoubleSoyCoffee =
                new Soy(new Soy(new DarkRoast()));

        System.out.println(myCoffee.getDescription());
        System.out.println(myCoffee.cost()); // .5 + .5 + .2 + .99

        System.out.println(myDoubleSoyCoffee.getDescription());
        System.out.println(myDoubleSoyCoffee.cost()); // .2+ .2 + .99
    }
}

