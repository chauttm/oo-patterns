package decorator.starbuzz;

/**
 * Created by dse on 01/12/2015.
 */
public class Soy extends Condiment {
    public Soy(Beverage beverage) {
        super(beverage, .20, "soy");
    }
}
