package decorator.starbuzz;

public class Condiment extends Beverage{
    public Beverage beverage;

    public double cost() {
        return cost + beverage.cost();
    }

    protected Condiment(Beverage beverage, double cost, String desc) {
        super(cost, desc);
        this.beverage = beverage;
    }

    @Override
    public String getDescription() {
        return description + " " + beverage.getDescription();
    }
}