package decorator.starbuzz;

/**
 * Created by dse on 01/12/2015.
 */
public class DarkRoast extends Beverage {
    public DarkRoast() {
        super(.89, "decorator.starbuzz.DarkRoast");
    }
}
