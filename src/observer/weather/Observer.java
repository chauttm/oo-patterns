package observer.weather;

/**
 * Created by dse on 24/11/2015.
 */
public interface Observer {
    public void update(int temperature, int humidity, float pressure);
}
