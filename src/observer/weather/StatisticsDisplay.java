package observer.weather;

/**
 * Created by dse on 24/11/2015.
 */
public class StatisticsDisplay implements Observer, Display {
    private final Subject weatherData;
    private float avg, max, min;
    private int count;

    public StatisticsDisplay(Subject weatherData) {
        weatherData.registerObserver(this);
        this.weatherData = weatherData;
        min = 10000;
    }

    @Override
    public void update(int temperature, int humidity, float pressure) {
        if (min > temperature) min = temperature;
        if (max < temperature) max = temperature;
        avg = ((avg * count) + temperature) / (count + 1);
        count++;
        display();
    }

    @Override
    public void display() {
        System.out.printf("Avg/Max/Min temperature = %.1f/%.1f/%.1f\n", avg, max, min);
    }
}