package observer.weather;

/**
 * Created by dse on 24/11/2015.
 */
public interface Subject {
    void registerObserver(Observer observer);
    void unRegisterObserver(Observer observer);
}
