package observer.weather;

/**
 * Created by dse on 24/11/2015.
 */
public interface Display {
    void display();
}
