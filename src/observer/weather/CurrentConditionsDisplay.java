package observer.weather;

/**
 * Created by dse on 24/11/2015.
 */
public class CurrentConditionsDisplay implements Observer, Display {

    private final Subject weatherData;
    private int temperature;
    private int humidity;

    public CurrentConditionsDisplay(Subject weatherData) {
        weatherData.registerObserver(this);
        this.weatherData = weatherData;
    }

    @Override
    public void update(int temperature, int humidity, float pressure) {
        this.temperature = temperature;
        this.humidity = humidity;
        display();
    }

    @Override
    public void display() {
        System.out.println("Current conditions: " + temperature +
            "F degrees and " + humidity + "% humidity");
    }
}
