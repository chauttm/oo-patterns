package observer.weather;

/**
 * Created by dse on 24/11/2015.
 */
public class ForecastDisplay implements Observer, Display{
    private final Subject weatherData;
    private String message;

    public ForecastDisplay(Subject weatherData) {
        weatherData.registerObserver(this);
        this.weatherData = weatherData;
    }

    @Override
    public void display() {
        System.out.println("Forcast: " + message);
    }

    @Override
    public void update(int temperature, int humidity, float pressure) {
        double r = Math.random();
        if (r > 0.66) message = "More of the same";
        else if (r > 0.33) message = "Watch out for cooler, rainy weather";
        else message = "Improving weather on the way!";
        display();
    }
}
