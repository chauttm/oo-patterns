package observer.weather;

import observer.weather.Observer;
import observer.weather.Subject;

import java.util.ArrayList;

/**
 * Created by dse on 24/11/2015.
 */
public class WeatherData implements Subject {
    private int temperature;
    private int humidity;
    private float pressure;
    private ArrayList<Observer> observers;

    public WeatherData(){
        observers = new ArrayList<Observer>();
    }

    public void setMeasurements(int temperature, int humidity, float pressure) {
        this.temperature = temperature;
        this.humidity = humidity;
        this.pressure = pressure;
        measurementsChanged();
    }

    public void measurementsChanged() {
        for (Observer observer: observers) {
            observer.update(temperature, humidity, pressure);
        }
    }

    @Override
    public void registerObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void unRegisterObserver(Observer observer) {
        observers.remove(observer);
    }
}
